import { Router } from 'express'
import 'isomorphic-fetch'

const router = Router()

router.get('/pie', (req, res) => {
  fetch('https://api.vtlibraries.us/api/v2/learning_test/_table/project?related=interaction_by_project_id', {
    method: 'GET',
    headers: {
      'Accept': 'application/json',
      'X-DreamFactory-Api-Key': process.env.TEST_KEY
    }
  })
  .then(response => {
    return response.json()
  })
  .then(response => {
    return response.resource.map(value => {
      return {
        'id': value.project_name,
        'label': value.project_name,
        'value': value.interaction_by_project_id.length
      }
    })
  })
  .then(response => {
    res.send(response)
  })
})

export const DataPackagesController = router
