import express from 'express'
import path from 'path'
import { WelcomeController, DataPackagesController } from './controllers'

const app = express()

const port = process.env.PORT || 3000

app.use('/welcome', WelcomeController)
app.use('/data', DataPackagesController)
app.use('/le', express.static(path.join(__dirname, '../le/build/')))
app.use('/tale', express.static(path.join(__dirname, '../tale/build/')))
app.use('/learning', express.static(path.join(__dirname, '../learning/build/')))

app.listen(port, () => {
  console.log(`Listening at http://localhost:${port}/`)
})
